- Get a Bookwork logo that has a transparent background.
- Get 3 short reference quotes from at least 2 other people. With their phone numbers and a small picture of them.
- Write a short one sentence summary of what you do.  This will be at the top of your resume.  Hint: it should be totally focused around Front end web development.  Words like responsive, HTML5, Javascript, CSS3, and Sass should be used.  That is if you have experience with those tecnologies ;)
- List out at least 4 Social tech groups with description
- Write one 2-3 sentence paragraph about your involvement in codepint.org.  Focus on how this is making the tech community in Cleveland a better place
- At least 1 to Job History entries that deal directly with web development.  Write out 3 bullet points under the this job entry.  Every bullet point should be front end related.  Meaning what you, in particular contributed to Bookwork by coding the Javascript, css, html5, etc.
- Eduction: Rewrite, either your "Research Assistant" position or the "Software Development" position. Make three bullet points for either one focusing on the coding that you did for each one. Make these bullet points as techy sounding as possible.  We initially talked about putting both of these on your resume, but I think only having one is fine.
- This one we are good on now.  Example website to the list below that you have coded.  
  1.) Bookwork
  2.) Scrape site - github repo
  3.) Rakeshguha.com

- List out all of your front end skills including CMS and github experience ( some backend skills are fine to ).  We did not talk about this in our phone conversation.  See the sample resume.  Write up as many bulleted skill points that you can think of.

